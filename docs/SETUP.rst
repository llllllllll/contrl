Certificate Authority:
======================

Edit the following CHANGE\_ME lines in order to run the bot: ( ^ at the
end denotes something that can be changed to '' to show it's not in use
)

1.  tor\_port ( Tor Socks Port ( 9050 by default for daemon, 9150 if
    using Browser Bundle ))
2.  server\_password ( Server-wide password ) ^ [
    http://askubuntu.com/questions/181111/how-can-i-set-ircd-hybrid-server-password
    ]
3.  nickserv\_pass ( NickServ password ) ^ but **DANGEROUS**
4.  channel\_ ( Channel to join ( must start with # - like #example ))
5.  server\_ ( Network to join ( irc.example.net ))
6.  port\_ ( Port number ( 6697 ))
7.  common\_name ( Hostname for the certificate ( Go to Pidgin's Buddy
    List -> Tools -> Certificates -> Get Info -> "Common name" for what
    to put here. You can also go to the bottom of this page and view the
    "Common Name" section.))
8.  cert\_ ( Where to find the certificate ) ^
9.  username\_ ( Bot nickname, change it to whatever you want )
10. sourceURL ( Source code for this bot ) ^
11. key ( Channel room password ) ^
12. finger\_var ( Owner information ) ^
13. versionNa\_var ( What is the name of this bot? ) ^
14. versionNu\_var ( Version of this bot ) ^
15. versionEn\_var ( What environment? Linux, Mac, etc ) ^
16. msg ( Verification message for commands. Think 'verify OpenSesame' )
    ^ if not using commands
17. user ( Username that is sending the verification message to the bot
    ) ^ if not using commands

Self-Signed:
============

Edit the following CHANGE\_ME lines in order to run the bot: ( ^ denotes
something that can be changed to '' to show it's not in use )

1.  tor\_port ( Tor Socks Port ( 9050 by default for daemon, 9150 if
    using Browser Bundle ))
2.  server\_password ( Server-wide password ) ^ [
    http://askubuntu.com/questions/181111/how-can-i-set-ircd-hybrid-server-password
    ]
3.  nickserv\_pass ( NickServ password ) ^ but **DANGEROUS**
4.  channel\_ ( Channel to join ( must start with # - like #example ))
5.  server\_ ( Network to join ( irc.example.net ))
6.  port\_ ( Port number ( 6697 ))
7.  common\_name ( Hostname for the certificate ( Go to Pidgin's Buddy
    List -> Tools -> Certificates -> Get Info -> "Common name" for what
    to put here. You can also go to the bottom of this page and view the
    "Common Name" section.))
8.  cert\_ ( Where to find the certificate ) Set to something like:
    /home/username/.purple/certificates/x509/tls\_peers/irc.example.net
9.  username\_ ( Bot nickname, change it to whatever you want )
10. sourceURL ( Source code for this bot ) ^
11. key ( Channel room password ) ^
12. finger\_var ( Owner information ) ^
13. versionNa\_var ( What is the name of this bot? ) ^
14. versionNu\_var ( Version of this bot ) ^
15. versionEn\_var ( What environment? Linux, Mac, etc ) ^
16. msg ( Verification message for commands. Think 'verify OpenSesame' )
    ^ if not using commands
17. user ( Username that is sending the verification message to the bot
    ) ^ if not using commands

NON-TLS:
========

Edit the following CHANGE\_ME lines in order to run the bot: ( ^ denotes
something that can be changed to '' to show it's not in use )

1.  tor\_port ( Tor Socks Port ( 9050 by default for daemon, 9150 if
    using Browser Bundle ))
2.  server\_password ( Server-wide password ) ^ [
    http://askubuntu.com/questions/181111/how-can-i-set-ircd-hybrid-server-password
    ]
3.  nickserv\_pass ( NickServ password ) ^ but **DANGEROUS**
4.  channel\_ ( Channel to join ( must start with # - like #example ))
5.  server\_ ( Network to join ( irc.example.net ))
6.  port\_ ( Port number ( 6667 ))
7.  common\_name ( Hostname for the certificate ) ^
8.  cert\_ ( Where to find the certificate ) ^
9.  username\_ ( Bot nickname, change it to whatever you want )
10. sourceURL ( Source code for this bot ) ^
11. key ( Channel room password ) ^
12. finger\_var ( Owner information ) ^
13. versionNa\_var ( What is the name of this bot? ) ^
14. versionNu\_var ( Version of this bot ) ^
15. versionEn\_var ( What environment? Linux, Mac, etc ) ^
16. msg ( Verification message for commands. Think 'verify OpenSesame' )
    ^ if not using commands
17. user ( Username that is sending the verification message to the bot
    ) ^ if not using commands

