#!/usr/bin/env python2.7
#
# Contrl  Copyright (C) 2014  5loth
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

# Checking if we have twisted, txsocksx, zope, and defs.py
# If we don't have these, obviously the bot will fail. 
# So if this is the case, do a sys.exit() and explain to
# the user what to read to properly setup the bot.
try:
    from twisted.internet.defer import Deferred
    from txsocksx.tls import TLSWrapClientEndpoint
    from zope.interface import implementer
    import defs
except ImportError:
    print('Bot is not properly setup.\n'
        + 'Go to https://bitbucket.org/contrl/contrl/src and click the\n'
        + '\'Setup\' file. Read the section that applies to your scenario.')
    sys.exit()

from twisted.internet.endpoints import SSL4ClientEndpoint
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.internet.protocol import ClientFactory
from twisted.internet.ssl import Certificate
from twisted.internet.ssl import optionsForClientTLS
from twisted.internet.task import react
from twisted.protocols.policies import SpewingFactory
from twisted.python.filepath import FilePath
from twisted.words.protocols.irc import IRCClient
from txsocksx.client import SOCKS5ClientEndpoint
import os
import sys
import time

# euid == 0 would mean the Linux / Mac user
# is running the bot as root. If so,
# we'll yell at the user and then sys.exit()
if sys.platform.startswith('linux'):
    if os.geteuid() == 0:
        print 'DO. NOT. RUN. AS. ROOT.'
        sys.exit()
    else:
        print 'Welcome to Contrl, Linux user!'
elif sys.platform.startswith('darwin'):
    if os.geteuid() == 0:
        print 'DO. NOT. RUN. AS. ROOT.'
        sys.exit()
    else:
        print 'Welcome to Contrl, Mac user!'
elif sys.platform.startswith('win32'):
# Just like the above, self-explanatory
    import ctypes
    if ctypes.windll.shell32.IsUserAnAdmin() == 1:
        print 'DO. NOT. RUN. AS. ADMINISTRATOR.'
        sys.exit()
    import win32api
    if win32api.GetUserName().upper() == 'SYSTEM':
        print 'DO. NOT. RUN. AS. SYSTEM.'
        sys.exit()
    else:
        print 'Welcome to Contrl, Windows user!'
        print 'TLS doesn\'t yet work :(. View issue #19 for info'
else:
    print 'Whatever OS you\'re running, it\'s not supported.'

os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))

# Filename where the
# TLS/SASL/Cert number is saved
sav = 'settings.ini'

# If the log folder doesn't exist, make it
if not os.path.isdir('logs'):
    os.mkdir('logs')

###
# Config
###
tor_port = CHANGE_ME
server_password = 'CHANGE_ME'
nickserv_pass = 'CHANGE_ME'
channel_ = '#CHANGE_ME'
server_ = 'CHANGE_ME'
port_ = CHANGE_ME
common_name = 'CHANGE_ME'
cert_ = 'CHANGE_ME'
username_ = 'CHANGE_ME'
sourceURL = 'CHANGE_ME'
key = 'CHANGE_ME'
finger_ = 'CHANGE_ME'
versionNa_ = 'CHANGE_ME'
versionNu_ = 'CHANGE_ME'
versionEn_ = 'CHANGE_ME'
###

# Does the settings file exist, and is bigger than 0?
if os.path.isfile(sav) and os.path.getsize(sav) > 0:
    with open(sav, 'r') as sav1:
        sav2 = sav1.readline()
# Check for self-signed
    try:
        if int(sav2) in [40, 42, 104, 106]:
            trustRootVar = Certificate.loadPEM(FilePath(cert_).getContent())
            defs.wipe_q()
# Check for certificate authority
        elif int(sav2) in [36, 38, 100, 102]:
            trustRootVar = None
            defs.wipe_q()
# Check for no tls
        elif int(sav2) in [16, 18, 80, 82]:
            trustRootVar = ''
            defs.wipe_q()
# Unrecognized number, wiping settings
        else:
            print 'Unknown option in ' + sav + ', wiping file.'
            with open(sav, 'w') as sav1:
                sav1.truncate(0)
            print 'Please run the program again.'
            sys.exit()
# Unrecognized number, wiping settings
    except ValueError:
        print 'Unknown option in ' + sav + ', wiping file.'
        with open(sav, 'w') as sav1:
            sav1.truncate(0)
        print 'Please run the program again.'
        sys.exit()
    except IOError:
        print 'Certificate settings are not properly setup.'
        with open(sav, 'w') as sav1:
            sav1.truncate(0)
        print 'Settings were wiped. Please run the program again.'
        sys.exit()
# Settings file either doesn't exist, or doesn't contain
# anything inside of it. Ask TLS/SASL/Certificate questions.
else:
    defs.question()
    with open(sav, 'r') as sav1:
        sav2 = sav1.readline()
# Check for self-signed
    try:
        if int(sav2) in [40, 42, 104, 106]:
            trustRootVar = Certificate.loadPEM(FilePath(cert_).getContent())
# Check for certificate authority
        elif int(sav2) in [36, 38, 100, 102]:
            trustRootVar = None
# Check for no tls
        elif int(sav2) in [16, 18, 80, 82]:
            trustRootVar = ''
    except ValueError:
        print 'Input was not Y, YES, N, or NO, stopping program.'
        sys.exit()
    except IOError:
        print 'Certificate settings are not properly setup.'
        with open(sav, 'w') as sav1:
            sav1.truncate(0)
        print 'Settings were wiped. Please run the program again.'
        sys.exit()

auth = 'auth.conf'
with open(auth, 'w') as auth_:
    auth_.truncate(0)

# If today's log doesn't exist, make it.
# Appending to the file further on in the
# program also creates the file if it
# doesn't exist.
if not os.path.isfile(time.strftime('./logs/%B %d, %Y.txt')):
    with open(time.strftime('./logs/%B %d, %Y.txt'), 'w') as log1:
        log1.truncate(0)

class IRC(IRCClient):

###
# Do not edit these.
# Instead, edit the CHANGE_ME options further up.
###
# Nick/Ident
    nickname = username_
# What is the real name of this bot?
    realname = nickname
# Used to authenticate to nickserv
    password = nickserv_pass
# Bot owner info
    fingerReply = finger_
# VERSION info
    versionName = versionNa_
    versionNum = versionNu_
    versionEnv = versionEn_
#
###

# CTCP FINGER
    def ctcpQuery_FINGER(self, user, channel, data):
        nick = user.split('!')[0]
        message_ = '[%s] %s sent CTCP FINGER' % (channel, nick)
        defs.log_file(message_)
        print defs.print_message(message_)
        if callable(self.fingerReply):
            reply = self.fingerReply()
        else:
            reply = str(self.fingerReply)
        self.ctcpMakeReply(nick, [('FINGER', reply)])

# CTCP SOURCE
    def ctcpQuery_SOURCE(self, user, channel, data):
        sourceURLvar = sourceURL
        nick = user.split('!')[0]
        message_ = '[%s] %s sent CTCP SOURCE' % (channel, nick)
        defs.log_file(message_)
        print defs.print_message(message_)
        if self.sourceURL:
            self.ctcpMakeReply(nick, [('SOURCE', sourceURLvar)])

# CTCP TIME
    def ctcpQuery_TIME(self, user, channel, data):
        nick = user.split('!')[0]
        message_ = '[%s] %s sent CTCP TIME' % (channel, nick)
        defs.log_file(message_)
        print defs.print_message(message_)
# GMT/UTC time is set here
        self.ctcpMakeReply(nick, [('TIME', ':%s' %
        time.asctime(time.gmtime(time.time())))])

# CTCP VERSION
    def ctcpQuery_VERSION(self, user, channel, data):
        nick = user.split('!')[0]
        message_ = '[%s] %s sent CTCP VERSION' % (channel, nick)
        defs.log_file(message_)
        print defs.print_message(message_)
        if self.versionName:
            self.ctcpMakeReply(nick, [('VERSION', '%s:%s:%s' %
            (self.versionName,
            self.versionNum or '',
            self.versionEnv or ''))])

# Read topic
    def irc_RPL_TOPIC(self, prefix, params):
        user = prefix.split('!')[0]
        channel = params[1]
        newtopic = params[2]
        message_ = '[%s] topic set by %s is \'%s\'' % (channel,
            user, newtopic)
        defs.log_file(message_)
        print defs.print_message(message_)
        self.topicUpdated(user, channel, newtopic)

# Topic is changed
    def irc_TOPIC(self, prefix, params):
        user = prefix.split('!')[0]
        channel = params[0]
        newtopic = params[1]
        message_ = '[%s] topic set by %s is now \'%s\'' % (channel,
            user, newtopic)
        if newtopic == '':
            message_ = '[%s] %s has cleared the topic' % (channel, user)
        defs.log_file(message_)
        print defs.print_message(message_)
        self.topicUpdated(user, channel, newtopic)

# Modes are added / removed
    def modeChanged(self, user, channel, set, modes, args):
        if set == False:
            setvar = '-'
        else:
            setvar = '+'
        nick = user.split('!')[0]
        message_ = '[%s] %s changed %s\'s mode to %s%s' % (channel,
            nick, args[0], setvar, modes)
        if args[0] == None:
            return None
        defs.log_file(message_)
        print defs.print_message(message_)

# Connecting to the server...
    def connectionMade(self):
        message_ = 'Connecting...'
        defs.log_file(message_)
        print defs.print_message(message_)
# SASL
        if int(sav2) in [18, 38, 42, 86, 102, 106]:
            self.sendLine('CAP REQ :sasl')
###

        self.deferred = Deferred()
        IRCClient.connectionMade(self)

# SASL
    def irc_CAP(self, prefix, params):
        if params[1] != 'ACK' or params[2].split() != ['sasl']:
            message_ = 'SASL not available, quitting'
            defs.log_file(message_)
            print defs.print_message(message_)
            self.quit('')
        sasl = ('{0}\0{0}\0{1}'.format(self.nickname,
            self.password)).encode('base64').strip()
        self.sendLine('AUTHENTICATE PLAIN')
        self.sendLine('AUTHENTICATE ' + sasl)

    def irc_903(self, prefix, params):
        self.sendLine('CAP END')

# This needs logged to file - TODO
    def irc_904(self, prefix, params):
        print 'SASL auth failed', params
        self.quit('')
    irc_905 = irc_904
###

# Connection lost
# This needs logged to file - TODO
    def connectionLost(self, reason):
        self.deferred.errback(reason)

# Signed onto network
    def signedOn(self):
        if int(sav2) in [16, 36, 40, 80, 100, 104]:
            message_ = 'Authenticating with nickserv'
            self.msg('NickServ','id %s' % nickserv_pass)
            defs.log_file(message_)
            print defs.print_message(message_)
        else:
            message_ = 'Authenticated with SASL'
            defs.log_file(message_)
            print defs.print_message(message_)
        self.sendLine('JOIN %s %s' % (channel_, key))

# Joined a channel
    def joined(self, channel):
        message_ = '[%s] joined' % channel
        defs.log_file(message_)
        print defs.print_message(message_)

# Who is in the channel with me?
    def irc_RPL_NAMREPLY(self, *nargs):
        message_ = '[' + str(nargs[1][2]) + '] /names: ' + nargs[1][3]
        defs.log_file(message_)
        print defs.print_message(message_)

# Kicked from channel, rejoin immediately
    def kickedFrom(self, channel, kicker, message):
        message_ = '[%s] kicked by %s' % (channel, kicker)
        defs.log_file(message_)
        print defs.print_message(message_)
        self.join(channel)

# Received notice
    def noticed(self, username, channel, message):
        username = username.split('!', 1)[0]
        if username == 'NickServ' and message == ' ':
            return None
        message_ = '[NOTICE] [%s] %s: ' % (channel,
            username) + '%s' % (message.replace('', ''))
        defs.log_file(message_)
        print defs.print_message(message_)

# Messages sent to channel ( or PM )
    def privmsg(self, user, channel, msg):
        user = user.split('!', 1)[0]
        user_ = user
        message_ = '[%s] %s: %s' % (channel, user, msg)
        defs.log_file(message_)
        print defs.print_message(message_)

#####
# Command Authentication
        if (channel == username_ and
        msg == 'CHANGE_ME' and
        user == 'CHANGE_ME'):
            defs.auth_user(self, user, channel)
# Commands
        if msg == "!op":
            with open(auth, 'r') as auth_:
                if any(line.strip() == user for line in auth_):
                    self.mode(channel, True, 'o', user=user)
        if msg == "!voice":
            with open(auth, 'r') as auth_:
                if any(line.strip() == user for line in auth_):
                    self.mode(channel, True, 'v', user=user)
#####

# Kicking a user
    def kick(self, channel, user, reason=None):
        message_ = '[%s] %s kicked' % (channel, user)
        defs.log_file(message_)
        print defs.print_message(message_)

### Observing users ###
# User joined channel
    def userJoined(self, user, channel):
        message_ = '[%s] %s joined' % (channel, user)
        defs.log_file(message_)
        print defs.print_message(message_)

# User changed nick
    def userRenamed(self, oldname, newname):
        message_ = '%s changed name to %s' % (oldname, newname)
        defs.log_file(message_)
        print defs.print_message(message_)
        defs.remove_user(user)

# /me actions
    def action(self, user, channel, data):
        message_ = '[%s] %s did action (%s)' % (channel, user, data)
        defs.log_file(message_)
        print defs.print_message(message_)

# User was kicked
    def userKicked(self, kickee, channel, kicker, message):
        message_ = '[%s] %s kicked %s with message ' % (channel,
            kicker, kickee) + '(%s)' % message
        user = kickee
        defs.log_file(message_)
        print defs.print_message(message_)
        defs.remove_user(user)

# User left channel
    def userLeft(self, user, channel):
        message_ = '[%s] %s left' % (channel, user)
        defs.log_file(message_)
        print defs.print_message(message_)
        defs.remove_user(user)

# User left network
    def userQuit(self, user, quitMessage):
        message_ = '%s left the network' % (user)
        defs.log_file(message_)
        print defs.print_message(message_)
        defs.remove_user(user)
###

class IRCFactory(ClientFactory):
    protocol = IRC

def main(reactor):
    host = common_name
# Tor localhost connection, and Tor port.
    torEndpoint = TCP4ClientEndpoint(reactor, '127.0.0.1', tor_port)
# TLS and Tor is added and removed here :-)
    if int(sav2) in [100, 102, 104, 106]:
        options_ = optionsForClientTLS(hostname = host.decode('utf-8'),
            trustRoot = trustRootVar,
            clientCertificate = None)
        ircEndpoint = SOCKS5ClientEndpoint(server_, port_, torEndpoint)
        tlsEndpoint = TLSWrapClientEndpoint(options_, ircEndpoint)
        d = tlsEndpoint.connect(SpewingFactory(IRCFactory()))
        d.addCallback(lambda proto: proto.wrappedProtocol.deferred)
# TLS is added and removed here :-)
    elif int(sav2) in [36, 38, 40, 42]:
        options_ = optionsForClientTLS(hostname = host.decode('utf-8'),
            trustRoot = trustRootVar,
            clientCertificate = None)
        nonTor_tlsEndpoint = SSL4ClientEndpoint(reactor, server_, port_, options_)
        d = nonTor_tlsEndpoint.connect(IRCFactory())
        d.addCallback(lambda proto: proto.deferred)
# If TLS was not chosen, don't use it.        
    elif int(sav2) in [80, 82]:
        ircEndpoint = SOCKS5ClientEndpoint(server_, port_, torEndpoint)
        d = ircEndpoint.connect(SpewingFactory(IRCFactory()))
        d.addCallback(lambda proto: proto.wrappedProtocol.deferred)
# If TLS, nor Tor was chosen, don't use either.
    elif int(sav2) in [16, 18]:
        nonTor_ircEndpoint = TCP4ClientEndpoint(reactor, server_, port_)
        d = nonTor_ircEndpoint.connect(IRCFactory())
        d.addCallback(lambda proto: proto.deferred)
    return d
react(main, [])