#!/usr/bin/env python2.7
#
# Contrl  Copyright (C) 2014  5loth
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.

#!/usr/bin/env python2.7
#
import sys
import time


sav = 'settings.ini'
auth = 'auth.conf'


# Tor / CA / SASL / Self-signed questions
def question():
    try:
        tor_q = str(raw_input('Would you like to use '
        + 'Tor? (Y)ES/(N)O: '))
    except ValueError:
        print 'Input was not Y, YES, N, or NO, stopping program.'
        sys.exit()
    if (tor_q.upper() == 'Y' or tor_q.upper() == 'YES'
    or tor_q.upper() == 'N' or tor_q.upper() == 'NO'):
        if tor_q.upper() == 'N' or tor_q.upper() == 'NO':
            tor_var = 0
        else:
            tor_var = 64
        try:
            tls_q = str(raw_input('Would you like to use '
            + 'TLS? (Y)ES/(N)O: '))
        except ValueError:
            print 'Input was not Y, YES, N, or NO, stopping program.'
            sys.exit()
        if (tls_q.upper() == 'Y' or tls_q.upper() == 'YES'
        or tls_q.upper() == 'N' or tls_q.upper() == 'NO'):
            if tls_q.upper() == 'N' or tls_q.upper() == 'NO':
                tls_var = 16
                try:
                    sasl_q = str(raw_input('Would you like to use '
                    + 'SASL authentication? (Y)ES/(N)O: '))
                except ValueError:
                    print 'Input was not Y, YES, N, or NO, stopping program.'
                    sys.exit()
                if (sasl_q.upper() == 'Y' or sasl_q.upper() == 'YES'
                or sasl_q.upper() == 'N' or sasl_q.upper() == 'NO'):
                    if sasl_q.upper() == 'N' or sasl_q.upper() == 'NO':
                        sasl_var = 0
                    else:
                        sasl_var = 2
                    with open(sav, 'w') as sav1:
                        sav1.write(str(tor_var + tls_var + sasl_var))
                        sav2 = tor_var + tls_var + sasl_var
            else:
                tls_var = 32
                try:
                    sasl_q = str(raw_input('Would you like to use '
                    + 'SASL authentication? (Y)ES/(N)O: '))
                except ValueError:
                    print 'Input was not Y, YES, N, or NO, stopping program.'
                    sys.exit()
                if (sasl_q.upper() == 'Y' or sasl_q.upper() == 'YES'
                or sasl_q.upper() == 'N' or sasl_q.upper() == 'NO'):
                    if sasl_q.upper() == 'N' or sasl_q.upper() == 'NO':
                        sasl_var = 0
                    else:
                        sasl_var = 2
                    try:
                        cert_q = str(raw_input('Would you like to connect '
                        + 'to a self-signed server? (Y)ES/(N)O: '))
                    except ValueError:
                        print 'Input was not Y, YES, N, or NO, stopping program.'
                        sys.exit()
                    if (cert_q.upper() == 'Y' or cert_q.upper() == 'YES'
                    or cert_q.upper() == 'N' or cert_q.upper() == 'NO'):
                        if cert_q.upper() == 'N' or cert_q.upper() == 'NO':
                            cert_var = 4
                        else:
                           cert_var = 8
                        with open(sav, 'w') as sav1:
                            sav1.write(str(tor_var + tls_var + sasl_var + cert_var))
                            sav2 = tor_var + tls_var + sasl_var + cert_var
                    else:
                        print 'Input was not Y, YES, N, or NO, stopping program.'
                        sys.exit()
                else:
                    print 'Input was not Y, YES, N, or NO, stopping program.'
                    sys.exit()
        else:
            print 'Input was not Y, YES, N, or NO, stopping program.'
            sys.exit()
    else:
        print 'Input was not Y, YES, N, or NO, stopping program.'
        sys.exit()
def wipe_q():
    try:
        wipe_qst = str(raw_input('Would you like to wipe settings? '
        + '(Y)ES/(N)O: '))
    except ValueError:
        print 'Input was not Y, YES, N, or NO, stopping program.'
        sys.exit()
    if (wipe_qst.upper() == 'Y' or wipe_qst.upper() == 'YES'
    or wipe_qst.upper() == 'N' or wipe_qst.upper() == 'NO'):
        with open(sav, 'r') as sav1:
            sav2 = sav1.readline()
        if wipe_qst.upper() == 'Y' or wipe_qst.upper() == 'YES':
            with open(sav, 'w') as sav1:
                sav1.truncate(0)
            print 'Settings were wiped. Please run the program again.'
            sys.exit()
        else:
            pass
    else:
        print 'Input was not Y, YES, N, or NO, stopping program.'
        sys.exit()


# Time used for printing to screen
def print_message(message_):
    return time.strftime('[%H:%M:%S on %B %d, %Y] ' + message_)


# Writing to file
def log_file(message_):
    with open(time.strftime('./logs/%B %d, %Y.txt'), 'a') as log1:
        log1.write(time.strftime('[%H:%M:%S] ' + message_ + '\n'))


def auth_user(self, user, channel):
    not_authenticated = None
    with open(auth, 'r') as auth_:
        if any(line.strip() == user for line in auth_):
            self.msg(user, 'You\'re already authenticated')
            message_ = '[{0}] {1} is already authenticated for commands'.format(
            channel, user)
            print_message(message_)
            log_file(message_)
        else:
            not_authenticated = True
        if not_authenticated == True:
            with open(auth, 'a') as auth_:
                auth_.write(user + '\n')
                not_authenticated = None
                self.msg(user, 'You\'re now authenticated')
            message_ = '[{0}] {1} is now authenticated for commands'.format(
            channel, user)
            print_message(message_)
            log_file(message_)


def remove_user(user):
    with open(auth, 'r') as auth_:
        lines = auth_.readlines()
        with open(auth, 'w') as auth_:
            for line in lines:
                if line.strip() != user:
                    auth_.write(line)