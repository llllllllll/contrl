Contrl IRC Bot
==============

Ability to:
-----------

- Connect to a CA or self-signed IRC server, through Tor
- Use TLS ( Linux / Mac supported. Windows TLS support Coming Soon\* )
- Log all actions/messages to a file, separated by day
- Authenticate with Nickserv or SASL
- Can be issued commands, and there is currently the following:
    - Voice
    - OP

\*https://bitbucket.org/contrl/contrl/issue/2/work-on-more-operating-systems


Current Version:
================

-  15.01 (2015, January)
- Stable Release
- A new version is released the first of every month
- If you'd prefer a more rapid release cycle, then try the 'Latest' branch.
    The most up-to-date version will always be in there.


WONTFIX:
========

-  None right now


Warning! Dragons Ahead!:
========================

This bot has only been tested on and proved to work with the following:

-  Linux: CrunchBang Waldorf, Debian 7.7, and Ubuntu 14.10
-  Windows: Vista, 7, (8).1
-  Mac: Needs testing

If your operating system does not work with the bot for one reason or
another, and you'd like help to test and get it working on your
operating system, please file an issue with traceback, version of OS,
and which version of the bot you were using. This way we can help you
and ensure more users can use this program!


Required:
=========

Debian and derivatives:

-  sudo apt-get install build-essential python-dev python-pip
   python-openssl
-  pip install --user idna service\_identity twisted txsocksx

Windows Vista/7/8:

-  pywin32 ( http://sourceforge.net/projects/pywin32/files/pywin32/ )
-  pip ( https://pip.pypa.io/en/latest/installing.html)
-  pip install idna service\_identity twisted txsocksx
-  TLS *DOES NOT* WORK! CHECK ISSUE #2


I'M HAVING PROBLEMS!:
=====================

-  1 Why am I getting

   ::

       sent = self._tlsConnection.send(toSend)
       OpenSSL.SSL.Error: [('SSL routines', 'SSL23_GET_SERVER_HELLO', 'unknown protocol')]

Because you're trying an insecure port but have opted to use TLS.

-  2 Why am I getting

   ::

       main function encountered error
       Traceback (most recent call last):
       Failure: twisted.internet.error.ConnectionDone: Connection was closed cleanly.

Because you're trying a secure port but have opted to not use TLS.


It's Implied That:
==================

-  **The server admin has prevented registration of the usernames
   "nickserv" and "chanserv" or has services installed.**
-  You're using Pidgin, if you have a self-signed server you're
   connecting to
-  If you're using commands, you'll use the bot in a channel that only
   allows users that are registered to join.


WTF Is With These Numbers?:
===========================

-  These are used in defs.py
- 0 + 16 + 0 = 16 NO TOR / NO TLS / NO SASL
- 0 + 16 + 2 = 18 NO TOR / NO TLS / YES SASL
- 0 + 32 + 0 + 4 = 36 NO TOR / YES TLS / NO SASL / NO SELF-SIGNED
- 0 + 32 + 2 + 4 = 38 NO TOR / YES TLS / YES SASL / NO SELF-SIGNED
- 0 + 32 + 0 + 8 = 40 NO TOR / YES TLS / NO SASL / YES SELF-SIGNED
- 0 + 32 + 2 + 8 = 42 NO TOR / YES TLS / YES SASL / YES SELF-SIGNED
- 64 + 16 + 0 = 80 YES TOR / NO TLS / NO SASL
- 64 + 16 + 2 = 82 YES TOR / NO TLS / YES SASL
- 64 + 32 + 0 + 4 = 100 YES TOR / YES TLS / NO SASL / NO SELF-SIGNED
- 64 + 32 + 2 + 4 = 102 YES TOR / YES TLS / YES SASL / NO SELF-SIGNED
- 64 + 32 + 0 + 8 = 104 YES TOR / YES TLS / NO SASL / YES SELF-SIGNED
- 64 + 32 + 2 + 8 = 106 YES TOR / YES TLS / YES SASL / YES SELF-SIGNED


CTCP TIME:
==========

In "CTCP TIME" there is

::

    #!python
    time.asctime(time.gmtime(time.time()))

and this is where we set gmt time by default.

Check out https://docs.python.org/2/library/time.html for information on
what to set for a specific time.


Common Name:
============

-  FREENODE: chat.freenode.net
-  OFTC: irc.oftc.net


**Have fun!**
